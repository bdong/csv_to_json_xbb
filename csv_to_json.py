import csv
import json
import os
import re
import util

# If you have the same structure of the csv files as in the example, all you need to modify is the following variables
csv_directory_path = 'Inputs/'
cut_file = 'cuts/'
systematics = 'systematics/'
output_json_path = 'Xbb_lookup_table.json'

tagger_name = "GN2Xv01"
jet_collection = "AntiKt10UFOCSSKSoftDropBeta100Zcut10Jets"

read_in_SFs = True

additional_data = {
    tagger_name: {
        jet_collection: {
            "meta": {
                "categories": ["hbb", "hcc", "top", "qcd"],
                "TaggingTarget": "hbb",
                "labelMapping": {
                    "Zbb": [3,11],
                    "ttbar": [1,6,7]
                },
                "TruthLabel": "R10TruthLabel_R22v1",
                "OperatingPoints": ["FlatMassQCDEff_0p25","FlatMassQCDEff_0p3","FlatMassQCDEff_0p37","FlatMassQCDEff_0p46","FlatMassQCDEff_0p58","FlatMassQCDEff_0p74","FlatMassQCDEff_0p94","FlatMassQCDEff_1p25","FlatMassQCDEff_1p55"],
                "fraction_hbb": 1.0,
                "fraction_hcc": 0.02,
                "fraction_top": 0.25,
                "fraction_qcd": 0.73,
            }
        }
    }
}

json_structure = {}
json_structure = {**additional_data, **json_structure}
target_dir = json_structure[tagger_name][jet_collection]
operating_points = additional_data[tagger_name][jet_collection]["meta"]["OperatingPoints"]
for wp in operating_points:
    util.read_in_cut_csv(csv_directory_path+cut_file, wp, target_dir, json_structure)
    if read_in_SFs:
        util.read_in_SFs_csv(csv_directory_path+systematics, wp, target_dir, json_structure)

with open(output_json_path, 'w') as jsonfile:
    json.dump(json_structure, jsonfile, indent=4, separators=(',', ': '))

