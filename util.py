import csv
import json
import os
import re
import sys


def extract_jet_type(filename):
    """This function takes in systematic csv file, based on the name of the csv file, check which label it is"""
    pattern = re.compile(r'SF_CDI_(.*?)_Xbb(.*?)\.csv')
    match = pattern.match(filename)
    if match:
        jet_type = match.group(1)
        return jet_type
    return None

def extract_pt_ranges(headers):
    """ Now we are inside the csv file for each truth label and WP, assuming the SF is pT dependent. Extract the pT range for each bin and the SFs """
    pt_ranges = []
    pt_pattern = re.compile(r'SF_pT_(\d+)_(\d+)')
    for header in headers:
        match = pt_pattern.match(header)
        if match:
            pt_ranges.append((int(match.group(1)), int(match.group(2))))

    pt_values = sorted(set([pt for range_pair in pt_ranges for pt in range_pair]))

    return pt_values

def read_in_cut_csv(csv_directory_path, wp, target_dir, json_structure):
    """ Take in the input csv cut file, and turn it into json"""
    if wp not in target_dir:
        target_dir[wp] = {}

    ## Just want to arrange pT bins in ascending order
    pt_files = []
    pt_bins = []
    for filename in os.listdir(csv_directory_path):
        pt_pattern = re.match(r'FlatMassDiscriminant_GN2Xv01_pT_(\d+)_(\d+|inf)\.csv', filename)
        if pt_pattern:
            pt_low, pt_high = pt_pattern.group(1), pt_pattern.group(2)

            # convert 'inf' to python's infinity for proper sorting
            pt_low = int(pt_low) if pt_low != 'inf' else float('inf')
            pt_high = int(pt_high) if pt_high != 'inf' else float('inf')

            pt_files.append((pt_low, pt_high, filename))
            pt_bins.append((pt_low, pt_high))
    pt_files.sort(key=lambda x: x[0])
    pt_bins = sorted(set([pt for range_pair in pt_bins for pt in range_pair]))

    if "pT_mass_2d_cutvalue" not in target_dir[wp]:
        target_dir[wp]["pT_mass_2d_cutvalue"] = {}
    inf_pt_bins = []
    for pt_value in pt_bins:
        if pt_value == float('inf'):
            pt_value = f"inf"
        inf_pt_bins.append(pt_value)
    target_dir[wp]["pT_mass_2d_cutvalue"]["pTbins"] = inf_pt_bins

    ## Now reading the cut values in each file
    for pt_low, pt_high, filename in pt_files:
        with open (os.path.join(csv_directory_path, filename)) as csv_file:
            csv_reader = csv.DictReader(csv_file)
            headers = csv_reader.fieldnames

            ## based on the specific column structure in the csv file
            mass_range_pattern = re.compile(r'\( M\(\)>=([0-9]+)\. && M\(\) < ([0-9]+)\.\)')
            mass_range_values = []
            cut_values = []
            for row in csv_reader:
                match = mass_range_pattern.search(row['WP'])
                if match:
                    mass_range_values.append((int(match.group(1)), int(match.group(2))))
                    cut_values.append(float(f"{float(row[wp]):.2f}"))
                else:
                    print("Can not find cut value and/or mass range for this WP. Please check the csv file.")
                    sys.exit(1)

            mass_range_values = sorted(set([mass for range_pair in mass_range_values for mass in range_pair]), reverse=True)
            pT_range_key = f"pT_{pt_low}_{pt_high}"
            if pT_range_key not in target_dir[wp]["pT_mass_2d_cutvalue"]:
                target_dir[wp]["pT_mass_2d_cutvalue"][pT_range_key] = {
                        "mass": mass_range_values,
                        "cutvalues":cut_values,
                    }
            else:
                print("You already filled out cut values for this pT bin [{}, {}], but you have another file for the same pT bin. This should not happen. Please have a look at your input files.".format(pt_low, pt_high))
                sys.exit(1)

    return json_structure

def read_in_SFs_csv(csv_directory, wp, target_dir, json_structure):
    """ Take in the input SF csv file, and turn it into json"""
    for csv_filename in os.listdir(csv_directory):
        if csv_filename.endswith(f"{wp[-2:]}.csv"):
            jet_type = extract_jet_type(csv_filename)
            if jet_type not in target_dir[wp]:
                target_dir[wp][jet_type] = {"pt": []}

            with open(os.path.join(csv_directory, csv_filename), newline='') as csv_file:
                csv_reader = csv.DictReader(csv_file)
                headers = csv_reader.fieldnames
                pt_values = extract_pt_ranges(headers)
                target_dir[wp][jet_type]["pt"] = pt_values[:]

                systematics = {}
                nominal_values = []
                for row in csv_reader:
                    syst_name = row['systs']
                    data_list = nominal_values if syst_name == "nominal" else systematics.setdefault(syst_name, [])
                    for pt_start, pt_end in zip(pt_values[:-1], pt_values[1:]):
                        header = f'SF_pT_{pt_start}_{pt_end}'
                        if header in row:
                            data_list.append(float(row[header]))
                    if nominal_values:
                        target_dir[wp][jet_type]["nominal"] = nominal_values
                    if systematics:
                        target_dir[wp][jet_type]["systematics"] = systematics
    return json_structure
