# csv_to_json_Xbb

how to run the script:
```
python3 csv_to_json.py
```

## Update config for new taggers
1. Make sure the cut values and also the calibration results have the same structure as the ones [here](https://gitlab.cern.ch/atlas-flavor-tagging-tools/software/csv_to_json_xbb/-/tree/master/Inputs).

2. After step 1, to have the JSON file for your new tagger, the only thing you need to modify is the configuration block in the [csv_to_json.py](https://gitlab.cern.ch/atlas-flavor-tagging-tools/software/csv_to_json_xbb/-/blob/master/csv_to_json.py#L8-37).

general option:
- `csv_directory_path`: path to the input folder
- `cut_file`: cut file input folder name
- `systematics`: systematic input folder name
- `output_json_path`: name of the output json file
- `tagger_name`: name of the tagger
- `jet_collection`: name of the jet collection

tagger metadata config inside the `additional_data` block:
- `categories`: output categories of the tagger
- `TaggingTarget`: tagging target
- `labeMapping`: how the labels are matched to different categories. For example, `"Zbb": [3,11]` means jet with truth label equals to 3 and 11 are classified into the `Zbb` category.
- `TruthLabel`: name of the variable that is used for truth labelling
- `OperatingPoints`: a list of the OPs that are supported of this tagger
- `fraction_hbb`: fraction value of the hbb.


## Input file naming scheme

In order for the code to work out of the box, the names for the input cut and systematic files must be in certain formants.

The name of the cut files must be: `FlatMassDiscriminant_[taggerName]_pT_[pTrange].csv` where `taggerName` is the name of the Xbb tagger; and `pTrange` is the pT range, for example, for pT range 1000 - 2000, it must to be written as `1000_2000`

The name of the systematics files has to be: `SF_CDI_[labelMaping]_Xbb[WP].csv` where `labelMapping` can be ttbar/Zbb/etc the truth categories that are calbirated; and `WP` is the defined working point